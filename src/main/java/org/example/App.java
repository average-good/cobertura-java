package org.example;

import java.security.InvalidParameterException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static final int sumPositives(int a, int b) {
        if (a < 0 || b < 0) {
            throw new InvalidParameterException("");
        }

        return a + b;
    }
}
